#!/usr/bin/env bash

if [ "$#" != 1 ]; then
  printf 'usage: ./build.sh <html output dir>\n' >&2
  exit 1
fi

set -exu

OUTPUT_DIRECTORY="$1"
R2_JSON_API=r2-ghapi.json

curl -sSL 'https://api.github.com/repos/radareorg/radare2/releases/latest' > "${R2_JSON_API}"

R2_VERSION=$(./parse-json-with-key.py tag_name < "${R2_JSON_API}")
R2_TARBALL_URL=$(./parse-json-with-key.py tarball_url <  "${R2_JSON_API}")
R2_BUILDDIR=builddir

# -- generate JSON compilation database ----------------------------------------

curl -sSL -O "${R2_TARBALL_URL}"
tar -xf "${R2_VERSION}"
R2_DIR=$(realpath "$(find ./ -maxdepth 1 -type d -name 'radareorg-radare2-*')")
meson setup --buildtype=release --strip "${R2_BUILDDIR}" "${R2_DIR}"

# -- generate pages -------------------------------------------------------------

codebrowser_generator \
    -b "${R2_BUILDDIR}/compile_commands.json" \
    -a \
    -o "${OUTPUT_DIRECTORY}" \
    -p "radare2:${R2_DIR}:${R2_VERSION}"
codebrowser_indexgenerator "${OUTPUT_DIRECTORY}"
