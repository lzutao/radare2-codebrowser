# radare2_codebrowser

Generate code browser for radare2 by [Woboq CodeBrowser][2].

[2]: https://github.com/woboq/woboq_codebrowser/

## Dependencies

- meson >= 0.46
- woboq-codebrowser => 2.1
- libllvm4.0

In Ubuntu 18 you could use these commands:

```sh
sudo apt-get install wget libllvm4.0
pip3 install --user meson
```

Download and install woboq-codebrowser

```sh
mkdir -p ~/builddir/tmp && cd ~/builddir/tmp
wget -O woboq.deb https://download.opensuse.org/repositories/home:/pansenmann:/woboq/Debian_9.0/amd64/woboq-codebrowser_2.1_amd64.deb
sudo dpkg -i woboq.deb
```

Grab the source code of radare2 and unzip it (current version is 3.0):

```sh
mkdir -p ~/builddir/ && cd ~/builddir
wget -O radare2.zip https://github.com/radare/radare2/archive/3.0.0.zip
unzip radare2.zip
```

## Build the code browser

Change directory to the extracted directory of radare2

```sh
OUTPUT_DIRECTORY=$HOME/public_html/codebrowser
DATA_DIRECTORY=$OUTPUT_DIRECTORY/../data
R2_VERSION=3.0
R2_DIR=$HOME/builddir/radare2-${R2_VERSION}
./woboq.sh "$R2_DIR" "$R2_VERSION" "$OUTPUT_DIRECTORY"
cp -rv /usr/share/woboq/data "$DATA_DIRECTORY"
```

## Set Up a Web Server

### Install nginx and configure it

```sh
sudo apt install nginx
```

Follow [the guide in linode][1].

[1]: https://www.linode.com/docs/websites/set-up-web-server-host-website/#set-up-a-web-server
