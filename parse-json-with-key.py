#!/usr/bin/env python3
import json
import sys

jdata = sys.stdin.read()

if sys.argv[1]:
    data = json.loads(jdata)
    field_value = data[sys.argv[1]]
    print(field_value)
